﻿$config = Get-Content '.\register.json' | Out-String | ConvertFrom-Json


ForEach ($reg in $config.registry)
{
   Set-ItemProperty -Path $reg.path -Name $reg.property -Value $reg.value
}

